import json
import requests


hostname = 'http://127.0.0.1:5000'

def handle_file_upload(file_path):
    with open(file_path, 'r') as file:
        json_data = json.load(file)
        return json_data

def make_api_call(api_url, json_data):
    headers = {'Content-Type': 'application/json'}
    response = requests.post(api_url, data=json.dumps(json_data),headers=headers)
    return response

def insertbyfile(file_path):
    chip_name = file_path.split('.')[0]
    json_data = handle_file_upload(file_path)
     # Adding chip_name to each key's object in the JSON data
    for key in json_data.keys():
        json_data[key]['chip_name'] = chip_name

    api_url = hostname+"/qubic/experiment"
    response = make_api_call(api_url, json_data)
    return response

def insertbyobject(json_data,chip_name):
    for key in json_data:
        json_data[key]['chip_name'] = chip_name
    api_url = hostname+"/qubic/experiment"
    response = make_api_call(api_url, json_data)
    return response

def geteachqubitdata(qubit_name):
    api_url = hostname+"/qubic/experiment/"
    response = requests.get(api_url+qubit_name)
    response_json_content = response.json()
    return response_json_content

def geteachqubitdata_chip(qubit_name,chip_name):
    api_url = hostname+"/dataVisulization/individual/qubit/properties/"
    response = requests.get(api_url+qubit_name+'/'+chip_name)
    response_json_content = response.json()
    return response_json_content

def getallqubitdata_property(property,chip_name):
    api_url = hostname+"/dataVisulization/"
    response = requests.get(api_url+property+'/'+chip_name)
    response_json_content = response.json()
    return response_json_content
