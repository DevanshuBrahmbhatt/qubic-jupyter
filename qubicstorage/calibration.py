import json
import requests
import ast


hostname = 'http://127.0.0.1:5000'

def handle_file_upload(file_path):
    with open(file_path, 'r') as file:
        json_data = json.load(file)
        return json_data

def make_api_call(api_url, api_data, commit_data, branch_name,chip_id):
    headers = {'Content-Type': 'application/json'}

    payload = {
        'data': api_data,
        'commit': commit_data,
        'branch_name': branch_name,
        'chip_id': chip_id
    }

    response = requests.post(api_url, headers=headers, json=payload)
    response_json_content = response.json()
    return response_json_content
    
def insertbyfile(file_path, commit_data, branch_name,chip_id):
    json_data = handle_file_upload(file_path)
    api_url = hostname+"/git/insert_data"
    response = make_api_call(api_url, json_data, commit_data, branch_name,chip_id)
    return response

def insertbyobject(json_data, commit_data, branch_name,chip_id):
    api_url = hostname+"/git/insert_data"
    response = make_api_call(api_url, json_data, commit_data, branch_name,chip_id)
    return response

def createbranch(commit_data, branch_name):
    requestData = {
        'branch_name': branch_name,
        'author_name': commit_data['author_name'],
        'author_email': commit_data['author_email'],
        'author_message': commit_data['author_message']
    }
    api_url = hostname+"/git/branches"
    response = requests.post(api_url, json=requestData)
    response_json_content = response.json()
    return response_json_content

def insertchip(chip_data, branch_name):
    requestData = {
        'branch_name': branch_name,
        'data':chip_data,
    }
    api_url = hostname+"/git/chip/insert_data"
    response = requests.post(api_url, json=requestData)
    response_json_content = response.json()
    return response_json_content

def getbranchdetails(branch_name):
    api_url = hostname+"/git/branch/commit_log/"
    response = requests.get(api_url+branch_name)
    response_json_content = response.json()
    return response_json_content

def getcommitdetails(hash, branch):
    api_url = hostname + "/git/get/commit/"
    response = requests.get(api_url + hash + '/' + branch)
    response_json_content = response.json()
    formatted_json = {"Qubits": {}, "Gates": {}}

    # Processing Qubit Data
    for qubit in response_json_content.get('qubit_data', []):
        qubit_name = qubit.get('name')
        if qubit_name:
            formatted_json["Qubits"][qubit_name] = {
                "freq": qubit.get('freq'),
                "readfreq": qubit.get('readfreq'),
                "freq_predicted":qubit.get('freq_predicted'),
                "freq_ef": qubit.get('freq_ef')
            }

    # Processing Gate Data
    for gate in response_json_content.get('gate_data', []):
        gate_name = gate.get('gate_name')
        if gate_name:
            if gate_name not in formatted_json["Gates"]:
                formatted_json["Gates"][gate_name] = []

            gate_entry = {
                "freq": gate.get('freq'),
                "phase": gate.get('phase'),
                "dest": gate.get('dest'),
                "twidth": gate.get('twidth'),
                "t0": gate.get('t0'),
                "amp": gate.get('amp'),
                # Parse 'env' if it's a JSON string, otherwise include it directly
                "env": json.loads(gate.get('env')) if gate.get('env') else gate.get('env'),
            }

            formatted_json["Gates"][gate_name].append(gate_entry)

    json_data = json.dumps(formatted_json, indent=4)
    with open(hash+'_commit_details.json', 'w') as file:
        file.write(json_data)

    return json_data

def getbranches():
    api_url = hostname+"/git/branches"
    response = requests.get(api_url)
    response_json_content = response.json()
    return response_json_content


def getchips(branch_name):
    api_url = hostname+"/git/get/chip/"
    response = requests.get(api_url+branch_name)
    response_json_content = response.json()
    return response_json_content

def getcommitdiff(hash,branch_name):
    api_url = hostname+"/git/get/diff/"
    response = requests.get(api_url+hash+ "/"+ branch_name )
    response_json_content = response.json()
    return response_json_content