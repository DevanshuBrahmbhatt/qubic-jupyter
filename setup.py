from setuptools import setup

setup(name='qubicstorage',
        packages=['qubicstorage'],
        version='0.0.1', 
        install_requires=[
            'requests'],
        author='Devanshu',
        author_email='dbrahmbhatt@lbl.gov',
        url='https://gitlab.com/DevanshuBrahmbhatt/qubic-jupyter',
        package_dir={'qubicstorage':'qubicstorage'})
